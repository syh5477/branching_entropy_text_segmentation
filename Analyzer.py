from NGramTable import NGramTable, UnseenPatternException

class UnseenFirstCharacterException(Exception):
    pass

class UnseenCurrentSubstringException(Exception):
    pass

class UnseenNextSubstringException(Exception):
    pass

class Analyzer:
    def __init__(self, n_gram, max_length, mode, val, verbose):
        """
        n_gram: n-gram table model
        max_length: n
        mode: 0 - Bmax / 1 - Bincrease
        val: valmax in Bmax / valdelta in Bincrease
        """
        self.n = max_length
        self.n_gram = n_gram
        self.verbose = verbose
        if mode == 0:
            self.b_seek = MaxBoundarySeeker(n_gram, val, verbose)
        elif mode == 1:
            self.b_seek = IncreaseBoundarySeeker(n_gram, val, verbose)
        else :
            raise Exception('Unexpected mode selection.')
    
    def do_segmentation(self, input_str):
        """
        input_str: encoded in utf-8
        return: set of boundary indices in input_str
        """
        b_set = set()

        x = list()
        offset = 0
        i = 0
        while i < self.n:
            if i < len(input_str):
                x.append(input_str[i])
                i += 1
            else:
                break

        while len(x) > 1 :
            if self.verbose:
                print(x)

            for j in range(2, len(x) + 1):
                if self.verbose:
                    print(j)
                try :
                    if j >= len(x) :
                        is_b = self.b_seek.is_boundary(''.join(x[0:j-1]), ''.join(x[0:j]), None)
                    else :
                        is_b = self.b_seek.is_boundary(''.join(x[0:j-1]), ''.join(x[0:j]), ''.join(x[0:j+1]))
                    
                    if is_b:
                        b_set.add(offset + j)
                        if self.verbose:
                            print('Boundary!')
                except UnseenFirstCharacterException:
                    break
                except UnseenCurrentSubstringException:
                    b_set.add(offset + j - 1)
                    if self.verbose:
                        print('Boundary Unseen Crnt!')
                    break
                except UnseenNextSubstringException:
                    b_set.add(offset + j)
                    if self.verbose:
                        print('Boundary Unseen Next!')
                    break
            
            x = x[1:]
            offset += 1

            if i < len(input_str):
                x.append(input_str[i])
                i += 1
        
        return b_set

class BoundarySeeker:
    def __init__(self, n_gram, verbose):
        self.n_gram = n_gram
        self.verbose = verbose

    def is_boundary(self, prev_str, crnt_str, next_str):
        return

class MaxBoundarySeeker(BoundarySeeker):
    def __init__(self, n_gram, valmax, verbose):
        self.valmax = valmax
        super().__init__(n_gram, verbose)

    def is_boundary(self, prev_str, crnt_str, next_str):
        # The case of considering only the last two substrings
        if next_str is None or len(next_str) < 1 :
            return False

        try:
            prev_val = self.n_gram.get_local_entropy(prev_str)
        except UnseenPatternException :
            raise UnseenFirstCharacterException
        
        try:
            crnt_val = self.n_gram.get_local_entropy(crnt_str)
        except UnseenPatternException :
            raise UnseenCurrentSubstringException

        try:
            next_val = self.n_gram.get_local_entropy(next_str)
        except UnseenPatternException :
            raise UnseenNextSubstringException
        
        if self.verbose:
            print('%f / %f / %f' % (prev_val, crnt_val, next_val))
        return crnt_val > max([self.valmax, prev_val, next_val])

class IncreaseBoundarySeeker(BoundarySeeker):
    def __init__(self, n_gram, valdelta, verbose):
        self.valdelta = valdelta
        super().__init__(n_gram, verbose)
    
    def is_boundary(self, prev_str, crnt_str, next_str):
        """
        next_str: not used (just for inheritance)
        """
        # prev_str is empty only if crnt_str is the first character.
        if prev_str is None or len(prev_str) < 1:
            return False
        
        try :
            prev_val = self.n_gram.get_local_entropy(prev_str)
        except UnseenPatternException :
            raise UnseenFirstCharacterException

        try :
            crnt_val = self.n_gram.get_local_entropy(crnt_str)
        except UnseenPatternException :
            raise UnseenCurrentSubstringException

        if self.verbose:
            print('%f / %f' % (prev_val, crnt_val))
        return crnt_val - prev_val > self.valdelta
