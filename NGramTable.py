from math import log2

class UnseenPatternException(Exception):
    pass

class NGramTable:
    def __init__ (self) :
        self.table = dict()

    def get_local_entropy(self, x_n):
        if x_n not in self.table:
            raise UnseenPatternException

        entry = self.table[x_n]
        total_cnt = entry[0]
        cnt = entry[1]

        if total_cnt <= 0:
            raise UnseenPatternException

        entropy = 0
        for x in cnt.keys() :
            cond_prob = cnt[x] / total_cnt
            entropy += cond_prob * log2(cond_prob)

        entropy = -entropy
        return entropy

    def update_cond_prob(self, x, x_n):
        if x_n not in self.table:
            self.table[x_n] = [0, dict()]
        
        entry = self.table[x_n]
        entry[0] = entry[0] + 1
        
        cnt = entry[1]
        if x not in cnt:
            cnt[x] = 1
        else :
            cnt[x] += 1
        
        return

    def reset(self):
        self.table = dict()