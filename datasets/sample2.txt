범에게 물려갔으나 정신만 바짝 차리면 살아나올 수 있는 하루이다.
1952년생 두 가지 소원은 이루기 어렵다.
한 가지에 매진하라.
1964년생 사공이 많으면 배가 산으로 가는 법이니 중요한 계약은 혼자 하라.
1976년생 해외여행이 꿈인데 몸이 따라 주지 않는다.
1988년생 생활에 고통 받는다.
남쪽에 있는 사람 귀인이다.
마음이 어지럽다.
기도함이 좋겠다.
1955년생 소원이 원대한 탓일까?
좀 더 치성을 드림이 좋겠다.
1967년생 어려운 일이 있을 때는 자존심을 버리고 남에게 도움을 청하라.
1979년생 금전상으로 약간의 손실이 있겠다.
1991년생 자신의 뜻이 굳고 변함이 없다면 능히 고난을 극복해 나갈 것이다.
YTN라디오 
박지원 국민의당 의원 특정인 '문재인'의 욕망으로 당 분열 돼 김종인 호남 베이스로 원대한 꿈꾸는 듯 김종인 꿍꿍이 속 있는 듯 김종인 전두환노태우 정권에서 호남연고 거론한 적 없어 김종인 호남 향해 뭐 하나 생각한 분 아냐 김종인 호남행?
문재인 전 대표 견제용 더민주 오너는 문재인 김종인 견딜 지 의문 김종인 세력 소수 당에서 힘쓰지 못할 것 문재인 당권대권 독점의식으로 야권 분열 온 것 야권연대 며칠 지나면 자연발생적으로 이뤄질 것 강봉균 영입?
여야 헷갈려..
여당에 유리할 것 박 대통령 공천 장악..
지난 주말에 더불어민주당 김종인 대표가 야권의 심장부라 불리는 호남을 찾아 '기득권을 가진 정치인들이 특정인의 욕망에 편승하면서 새로운 정치를 이룩할 수 있는 것처럼 착각하고 있다'며 국민의당을 거듭 비판했는데요.
이런 부분을 비롯해서 지금의 정치 상황 어떻게 바라보고 있는지 국민의당의 대표적인 호남 정치인이죠.
박지원 의원 전화로 연결해 이야기 나눠보겠습니다.
안녕하십니까?
네 안녕하세요.
목포에 있습니다.
아 그러시군요.
그런데 김종인 대표가 이 말 한 거요.
'특정인의 욕망을 채우기 위해서 당이 분열로 갔다.' 이런 이야기 어떻게 보세요?
맞는 이야기예요.
그 특정인이 바로 문재인 전 대표입니다.
아 그렇군요.
그럼 호남에 가서 이런 이야기를 한 이유가 뭘까요?
글쎄요.
뭐 김종인 대표께서는 아마 총선 후에도 자기 스스로가 킹메이커는 하지 않겠다고 했기 때문에 자기가 어떤 원대한 꿈을 가지고 갑자기 호남을 홈베이스로 만들어가지고 무슨 생각을 해보는 것 아닌가?
이런 생각이 듭니다.
아 진짜 무슨 생각을 한다고 보세요?
네 문재인 전 대표가 그렇게 모셔왔고 사실상 더민주당은 친노들이 다수거든요.
아무리 이번에 청산을 했다고 하더라도 실질적으로 친노 젊은 세력이 다 지배를 하고 있는데 갑자기 그 런 이야기를 하는 것은 무슨 꿍꿍이속이 있지 않는가?
저는 그런 생각을 합니다.
아 본인의 야망이 있다 이런 말씀이시군요?
뭐 정치하는 사람이라면 다 본인의 야망이 있죠.
그렇지만 좀 생뚱맞은 이야기를 하는 것은 옳지 않죠.
왜냐면 김종인 대표를 저도 호형호제하고 잘 알아요.
그러나 수십 년간 알고 지내면서 당신이 광주에서 초등학교 중학교 다녔다는 이야기를 한 번도 안 했어요.
그리고 지금까지 호남의 연고를 가지고 있는 것은 물론 조부님 할아버지께서 전북 순창 출신이시기 때문에 그런 것을 언론에서 이야기는 했지만 본인이 그러한 것을 이야기한 적도 없고 사실 국보위 이래 전두환 노태우 정권에서 고위직을 다 역임했지 않습니까?
이러한 때 호남을 향해서 지금까지 정치를 하면서 호남을 향해서 아무런 이야기를 하지 않고 참 뭐 하나 생각해주신 분이 아니거든요.
그런데 갑자기 와서 자기가 호남을 대변할 수 있다 지금까지 기득권을 가진 사람들이 당을 분열시켰다 이런 것은 문재인 대표를 견제하는 것 아닌가?
저는 그렇게 생각합니다.
그런데 저는 궁금한 게 왜 여태까지 광주에서 학교 다닌 이야기를 하지 않았을까요?
그런 때야 전두환 노태우 정권에서는 호남을 고향으로 가졌거나 그러면 불이익을 당하니까 이야기를 안 했겠죠.
사실 김대중 전 대통령이 집권할 때 커밍아웃하신 분이 많아요.
그만큼 군사독재정권에서 호남을 소외시켰기 때문에 자기들이 살기 위해서 그러한 것도 저는 인정을 합니다.
그걸 나쁘다고 생각하지는 않아요.
그렇다고 한다면 이제 와서 자기가 그런 고위직에 있을 때 호남을 조금 더 보살피지 못한 것에 대해서 솔직히 죄송하다 그러나 지금부터라도 내가 반성해서 내 고향이니까 호남을 대변하는 데에 앞장서겠다 이런 태도가 좋은 거 아니에요?
이번에 오히려 새누리당 김무성 대표는 국민들에게 상향식 공천을 수십 차례 약속했는데 이걸 지켜내지 못하고 사실상 박근혜 대통령에게 꿇은 거 아니에요?
그걸 지켜내지 못한 것을 죄송하게 생각한다고 솔직히 인정하고 사과하니까 저는 참 좋더라고요.
지금 김종인 대표가 이런 이야기도 하지 않았습니까?
'총선이 끝나도 더불어민주당이 옛날 모습으로 돌아가는 일은 절대 없을 것이다.
변화하고 있다.' 이렇게 단언하는데 이 부분은 어떻게 보세요?
그것도 나는 좋은 방법이라고 생각합니다.
옛날처럼 친노 패권주의로 장악해가지고 그러는 것은 막아야겠다 이런 것은 긍정적으로 평가하지만 정치는 현실인데 김종인 대표가 지금 문재인 전 대표가 필요해서 바지사장으로 모셔온 것 아니에요?
오너는 문재인 전 대표예요.
본인은 아니라고 하죠.
실제로 친노가 움직이고 있다는 말입니다.
그런데 그렇게 하지 않겠다고 하는 것은 저는 긍정적으로 평가를 합니다.
그런데 과연 그것이 실현될까?
또 그러한 힘을 가지고 견뎌낼 수 있을까 하는 것은 조금 더 두고 볼 문제죠.
그렇군요.
그런데 어쨌든 문재인 전 대표하고 김종인 대표하고 투톱으로 역할분담을 하는 것 같은데요.
문재인 전 대표가 그런 이야기 하지 않았습니까?
마포에 가서 정청래 의원 낙천된 것 도저히 받아들일 수 없는 공천이었다 이런 식으로 이야기를 한 것 같은데요.
이런 것도 표를 얻기 위해서 정체성 논란 같은 경우에도 문재인 대표는 김종인 대표하고 다른 입장을 표명하지 않았습니까?
이런 것도 선거 전략이라고 봐야 하나요?
어떻게 생각하세요?
저는 선거 전략이라고 보고요.
문재인 대표의 평소 생각을 반영해서 하신 발언이라고 생각합니다.
김종인 대표가 한 것이 100 옳은 일은 아니거든요.
또 제가 한 일이 100 옳다고 주장할 수도 없는 겁니다.
그렇기 때문에 문재인 대표로서는 사실 정청래 의원 같은 경우에는 한 쪽에서는 비난도 하지만 야당으로서 굉장히 필요한 인사입니다.
상당히 공격적이고 항상 집권 여당 청와대에 대해서 지금 아무런 이야기 못하고 있잖아요?
지금 박근혜 대통령이 그 무서운 새누리당의 공천을 좌지우지하고 있는데도 김종인 대표가 박근혜 대통령을 향해서 하는 말씀이 하나라도 있어요?
결국 주장하는 게 경제민주화 운운하는데 사실 경제민주화에 대해서 김종인 대표가 그러한 주장을 했지만 그분이 의정활동을 하면서 경제민주화에 대해서 입법 활동 하나도 하지 않은 분이에요.
그러니까 늘 말로만 주장하는데 저는 문재인 전 대표가 정청래 의원 지역에 가서 하신 말씀은 다분히 전략적이기도 하고 또 김종인 대표가 정체성을 너무 버리고 가니까 거기에 대한 하나의 제동도 하는구나 그렇게 좋은 의미로 받아들였습니다.
그런데 김종인 대표하고 문재인 전 대표하고 관계가 총선 이후에는 갈등으로 번질 가능성이 있다 그래서 둘이 한판 붙는다 이런 이야기가 있는데요.
그런데 이게 세력으로 볼 때 붙을 수 있는 상황은 아니지 않습니까?
글쎄요.
정치인은 그 자리에 가면 다 하고 싶으니까 김종인 대표도 무슨 꿈을 가지고 있겠죠.
그리고 그러한 뉘앙스가 있는 말씀도 많이 하시는데요.
그렇더라도 어려울 거예요.
그런 세력이 안 되잖아요?
물론 몇 사람에 대해서 공천에 영향력을 행사했고 특히 비례대표 같은 것에 영향력을 행사했다고 하더라도 김종인 대표 세력은 소수에 불과합니다.
그리고 실제로 지방의 당원들 이런 분들이 친노 세력이 강하기 때문에 그렇게 힘쓰지 못할 겁니다.
어차피 문재인 대표가 하는 거예요.
그렇군요.
대선후보로서도 문재인 대표가 당내에서 대선가도에 문제가 없다?
그렇죠?
당내에서는 아무래도 강자가 될 수 있지만 제가 볼 때는 정권교체를 위해서는 문재인 대표 그게 바로 저하고 문재인 대표하고 지난 2월 전당대회에서 갈린 것인데요.
자기들은 당을 장악해서 문재인 대표가 당권도 가지고 대권도 가져야겠다 하는 생각 때문에 오늘의 분열이 온 겁니다.
그렇기 때문에 대권에 대해서는 야권이 모든 사람에게 다 오픈해놓고 링 위에 올라와서 치열한 경쟁을 해가지고 거기서 국민의 검증을 받고 당원이 인정하는 그런 대통령 후보가 되어야 하는데 지금도 친노 문재인 대표 측에서는 문재인 대표만이 대권후보가 되어야 한다는 생각을 가지고 있는 거예요.
그렇기 때문에 김종인 대표는 그렇게 해서는 현실적으로 이번 총선에 어려움이 있기 때문에 두 분이 역할분담을 해서 말씀을 맞췄는지 안 맞췄는지 그건 저는 모르겠습니다.
