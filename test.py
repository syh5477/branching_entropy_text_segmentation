import copy

s = "한국인의밥상"
a = "한국인-의-밥상"
b_set = set()
b_set.add(3)
b_set.add(4)

b_list = list(b_set)
sorted(b_list)
print(b_list)

s_list = list(s)
num_inserted = 0
for i in b_list:
    s_list.insert(i + num_inserted, '-')
    num_inserted += 1

print(''.join(s_list))
print(a)


def fancy_format(s, f_b_set, b_b_set, delim = '-') :
    b_set = copy.deepcopy(f_b_set)
    for b in b_b_set:
        b_set.add(len(s) - b)
    
    b_list = list(b_set)
    s_list = list(s)
    
    n_inserted = 0
    for b in b_set :
        s_list.insert(b + n_inserted , delim)
        n_inserted += 1
    
    return ''.join(s_list)