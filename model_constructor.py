import pickle

from NGramTable import NGramTable, UnseenPatternException
from Constructor import Constructor

f_list = ['bigkinds_space_forward', 'bigkinds_space_backward', 'bigkinds_nospace_forward', 'bigkinds_nospace_backward']

# Model construction
for name in f_list:
    print('%s processing...' % name)

    input_file = open('datasets/%s.txt' % name, mode='r', encoding='utf8')
    cons = Constructor(8)
    cons.update(input_file)
    model = cons.construct_table()
    input_file.close()

    output_file = open('models/%s.mdl' % name, mode='wb')
    output_file.write(pickle.dumps(model))
    output_file.close()

    print('%s done' % name)