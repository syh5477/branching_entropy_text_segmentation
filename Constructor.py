from NGramTable import NGramTable
class Constructor:
    def __init__(self, max_length) :
        self.n_gram = NGramTable()
        self.n = max_length
        
    def construct_table(self):
        return self.n_gram

    def update(self, input_file):
        '''
        input_file: encoded by 'utf-8'
        '''
        for line in input_file:
            line = line[:-1] # Delete the new line character
            self.update_line(line)

    def update_line(self, input_str):
        x = list()

        i = 0
        while i < self.n:
            if i < len(input_str):
                x.append(input_str[i])
                i += 1
            else:
                break

        while len(x) > 1:
            for j in range(len(x)-1):
                self.n_gram.update_cond_prob(x[j+1], ''.join(x[0:j+1]))
            
            x = x[1:]
            if i < len(input_str):
                x.append(input_str[i])
                i += 1


