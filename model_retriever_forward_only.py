import pickle
import copy
import time
import sys

from NGramTable import NGramTable
from Analyzer import Analyzer

def fancy_format(s, b_set, delim = '-') :
    b_list = list(b_set)
    sorted(b_list)
    s_list = list(s)

    n_inserted = 0
    for b in b_list :
        s_list.insert(b + n_inserted , delim)
        n_inserted += 1
    
    return ''.join(s_list)

if len(sys.argv) != 6 :
    print('Wrong args: model_retriever.py <1> <2> <3> <4> <5>')
    print('<1>: 0 - Space / 1 - NoSpace')
    print('<2>: 3~6 - Max Length')
    print('<3>: 0 - Bmax / 1 - Bincrease')
    print('<4>: valmax in Bmax / valdelta in Bincrease')
    print('<5>: 0 - No Debug / 1 - Debug')
    exit()

if sys.argv[1] == '0' :
    model_kind = 'space'
elif sys.argv[1] == '1' :
    model_kind = 'nospace'
else :
    print('<1>: 0 - Space / 1 - NoSpace')
    exit()

max_length = int(sys.argv[2])
if max_length < 3 or max_length > 6:
    print('<2>: 3~6 - Max Length')
    exit()

mode_num = int(sys.argv[3])
if mode_num < 0 or mode_num > 1:
    print('<3>: 0 - Bmax / 1 - Bincrease')
    exit()

val = float(sys.argv[4])

if sys.argv[5] == '0' :
    verbose = False
elif sys.argv[5] == '1' :
    verbose = True
else :
    print('<5>: 0 - No Debug / 1 - Debug')
    exit()

if verbose:
    start = time.time()

f_model_src = open('models/bigkinds_%s_forward.mdl' % model_kind, mode='rb')
f_model = pickle.load(f_model_src, encoding='utf8')
f_model_src.close()

if verbose:
    end = time.time()
    print(end - start)

f_anlyzr = Analyzer(f_model, max_length, mode_num, val, verbose)

with open('testsets/testset_%s.txt' % model_kind, mode='r', encoding='utf8') as test_file :
    output_file = open('results/%s_%d_%d_%.2f.txt' % (model_kind, max_length, mode_num, val), mode='w', encoding='utf8')

    for line in test_file:
        s = line[:-1]
        s_r = s[::-1]

        if verbose:
            print(s)
            print(s_r)

        b_set = f_anlyzr.do_segmentation(s)

        if verbose:
            print(b_set)

        b_list = list(b_set)
        sorted(b_list)

        for b in b_list :
            output_file.write('%d,' % b)
        output_file.write('\n')
        output_file.write(fancy_format(s, b_set))
        output_file.write('\n')
    
    output_file.close()